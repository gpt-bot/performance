# GitLab Performance Tool - Running the Tests

* [GitLab Performance Tool - Preparing the Environment](environment_prep.md)
* [**GitLab Performance Tool - Running the Tests**](k6.md)

On this page we'll detail how to configure and run [k6](https://k6.io/) tests against a GitLab environment with the GitLab Performance Tool (gpt). Note that we assume you have a working knowledge of [k6](https://k6.io/) and [Docker](https://www.docker.com/).

**Note: Before running any tests with the Tool, the intended GitLab environment should be prepared first. Details on how to do this can be found here: [GitLab Performance Tool - Preparing the Environment](environment_prep.md)**

* [Tool Requirements](#tool-requirements)
* [Configuring the Tool](#configuring-the-tool)
  * [Environments](#environments)
  * [Options](#options)
  * [Tests](#tests)
    * [Test Types](#test-types)
    * [Unsafe Tests](#unsafe-tests)
* [Running the Tests with the Tool](#running-the-tests-with-the-tool)
  * [Docker (Recommended)](#docker-recommended)
  * [Linux](#linux)
  * [Location and Network conditions](#location-and-network-conditions)
  * [Test Output and Results](#test-output-and-results)
    * [Test Thresholds](#test-thresholds)
    * [Evaluating Failures](#evaluating-failures)
    * [Comparing Results](#comparing-results)
* [Troubleshooting](#troubleshooting)
  * [`socket: too many open files`](#socket-too-many-open-files)

## Tool Requirements

The minimum hardware and software requirements for running the Tool are dependent on the intended load target (given as Requests per Second or RPS), the underlying hardware and the complexity of the tests being run.

As a rule of thumb, we recommend that to run the default tests at **200 RPS** the machine running the Tool has approximately **1 CPU Core** and **1 GB RAM** available.

So for example, to run the default tests at the following RPS targets the machine should have these specs available as a minimum:
* 200 RPS - 1 Core CPU, 1 GB RAM
* 500 RPS - 2 Core CPU, 2.5 GB RAM
* 1000 RPS - 4 Core CPU, 5 GB RAM

We recommend running the tool with [Docker](https://www.docker.com/) but the tool can also be run on Linux natively. More details can be found in the [Running the Tests with the Tool](#running-the-tests-with-the-tool) section below.

## Configuring the Tool

Before you can run performance tests with the tool against your GitLab environment you need to configure it. This involves configuring the tool with all of the required info about your environment, the project(s) you intend to test against, any additional tests you wish to run and optionally to run the tests.

There are three key pieces of configuration in the tool - [Environments](../k6/environments), [Options](../k6/options) and [Tests](../k6/tests). This section details each as follows.

Out of the box all the default k6 tests are configured to run against an environment that has at least one instance of the default Test Project setup (see [GitLab Performance Tool - Preparing the Environment](environment_prep.md) for more info). However, this is configurable and the tool can be configured to run against any project providing it has equivalent data to test (more details in the next section).

### Environments

The first piece of configuration is the [Environment Config File](../k6/environments). This file should contain the environment's information, such as it's URL, as well as the required details for each project the tests will run against.

As an example, the following is one of our Environment Config Files, [`10k.json`](../k6/environments/10k.json), that configures the tests on where to find the Environment and what Projects to test against:

```json
{
  "environment": {
    "name": "10k",
    "url": "http://10k.testbed.gitlab.net"
  },
  "projects": [
    {
      "name": "gitlabhq",
      "group": "qa-perf-testing",
      "branch": "10-0-stable",
      "commit_sha": "0a99e022",
      "commit_sha_signed": "6526e91f",
      "compare_commits_sha": ["aec887ab", "5bfb7558"],
      "file_path": "qa%2fqa%2erb",
      "git_push_data": {
        "branch_current_head_sha": "8606c89683c913641243fc667edeb90600fe1a0e",
        "branch_new_head_sha": "8bcb4fd6f5780ebe9dc1ec80904b060b89a937d2",
        "branch_name": "12-1-auto-deploy-20190714"
      },
      "mr_commits_iid": "10495",
      "mr_discussions_iid": "6958",
      "user": "root"
    },
    {
      "name": "gitlabhq2",
      "group": "qa-perf-testing",
      "branch": "10-0-stable",
      "commit_sha": "0a99e022",
      "commit_sha_signed": "6526e91f",
      "compare_commits_sha": ["aec887ab", "5bfb7558"],
      "file_path": "qa%2fqa%2erb",
      "git_push_data": {
        "branch_current_head_sha": "8606c89683c913641243fc667edeb90600fe1a0e",
        "branch_new_head_sha": "8bcb4fd6f5780ebe9dc1ec80904b060b89a937d2",
        "branch_name": "12-1-auto-deploy-20190714"
      },
      "mr_commits_iid": "10495",
      "mr_discussions_iid": "6958",
      "user": "root"
    }
  ]
}
```

* The environment's Name and URL.
    * Note as a convenience these two settings can also be defined as environment variables, `ENVIRONMENT_NAME` and `ENVIRONMENT_URL` respectively, for overriding as required.
* Details for each project that the tests should target along with it's data. You should aim to have each of these details present here and in the target environment otherwise the specific tests that require them will be skipped automatically:
    * `name` - Name of the Project.
    * `group` - The name of the Group that contains the intended Project.
    * `branch` - The name of a large branch available in the project. The size of the branch should be tuned to your environment's requirements.
    * `commit_sha` - The SHA reference of a large commit available in the project. The size of the commit should be tuned to your environment's requirements.
    * `commit_sha_signed` - The SHA reference of a [signed commit](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/) available in the project.
    * `compare_commits_sha` - The SHA references of two commits on the same branch that will be [compared](https://docs.gitlab.com/ee/api/repositories.html#compare-branches-tags-or-commits). The difference between the commits should be tuned to your environment's requirements.
    * `file_path` - The relative path to a normal sized file in your project.
    * `git_push_data` - Git push data that will be used for git push test. No need to change anything if you're using `gitlabhq`. To test a custom project or learn more about git push test, please refer to [`Git Push test documentation`](test_docs/git_push.md). The size of the commits should be tuned to your environment's requirements.
      * `branch_current_head_sha` - The head commit of the `branch_name` branch.
      * `branch_new_head_sha` - Any commit SHA that older then `branch_current_head_sha` on the `branch_name` branch.
      * `branch_name` - Existing branch name.
    * `mr_commits_iid` - The [iid](https://docs.gitlab.com/ee/api/#id-vs-iid) of a merge request available in the project that has a large number of commits. The size of the MR should be tuned to your environment's requirements.
    * `mr_discussions_iid` - The [iid](https://docs.gitlab.com/ee/api/#id-vs-iid) of a merge request available in the project that has a large number of discussions / comments. The size of the MR discussions should be tuned to your environment's requirements.
    * `user` - The name of a valid user for testing related endpoints.

### Options

The second piece of configuration is the [Options Config File](../k6/options). This file will set how the tool will run the tests - e.g. how long to run the tests for, how many users and how much throughput.

The [Options Config Files](../k6/options) are themselves native [k6 config files](https://docs.k6.io/docs/options). For this tool, we use them to set options but they can also be used to set any valid k6 options as required for advanced use cases.

As an example, the following is one of our Options Config Files, [`20s_2rps.json`](../k6/options/20s_2rps.json), that configures the tests to each run for 20 seconds at a rate of 2 Requests Per Second (RPS):

```json
{
  "stages": [
    { "duration": "5s", "target": 20 },
    { "duration": "10s", "target": 20 },
    { "duration": "5s", "target": 0 }
  ],
  "rps": 20,
  "batchPerHost": 0,
  "maxRedirects": 0
}
```

* `stages` - Defines the stages k6 should run the tests with. Sets the duration of each stage and how many users (VUs) to use. 
    * It should be noted that each stage will ramp up from the previous, so in this example the scenario is to ramp up from 0 to 2 users over 5 seconds and then maintain 2 users for another 15s.
* `rps` - Sets the maximum Requests per Second that k6 can make in total.
* `batchPerHost` - Sets k6 to allow unlimited requests to the same host in [batch](https://docs.k6.io/docs/batch-requests) calls.
* `maxRedirects` - Sets the max number of redirects k6 will follow to none.

Note that it's best practice to set the number of users (VUs) to the same amount as RPS to ensure the target RPS can be reached.

### Tests

Finally we have the third and last piece of configuration - the [k6 test scripts](https://docs.k6.io/docs/running-k6#section-executing-local-scripts). Each file contains a test to run against the environment along with any extra config such as setting thresholds.

To get more detailed information about the current test list you can refer to the [Current Test Details wiki page](https://gitlab.com/gitlab-org/quality/performance/wikis/current-test-details).

Like Options, test files are native [k6 test scripts](https://docs.k6.io/docs/running-k6#section-executing-local-scripts) and all valid k6 features and options can be used here.

With the tool, we provide various curated tests and libraries that are designed to performance test a wide range of GitLab functions. We continue to iterate and release tests also. In each test we set the actions to take (e.g. call an API) along with defining thresholds that determine if the test is a success (e.g. actual RPS should be no less than 20% of the target and no more than 5% of requests made can be failures).

As an example, the following is one of our API Tests, [`api_v4_projects_project.js`](../k6/tests/api/api_v4_projects_project.js), that tests the [GET Single Project API](https://docs.gitlab.com/ee/api/projects.html#get-single-project):

```js
/*global __ENV : true  */
/*
@endpoint: `GET /projects/:id`
@description: [Get single project](https://docs.gitlab.com/ee/api/projects.html#get-single-project)
*/

import http from "k6/http";
import { group, fail } from "k6";
import { Rate } from "k6/metrics";
import { logError, getRpsThresholds, getTtfbThreshold, getProjects, selectProject } from "../../lib/gpt_k6_modules.js";

if (!__ENV.ACCESS_TOKEN) fail('ACCESS_TOKEN has not been set. Skipping...')

export let rpsThresholds = getRpsThresholds()
export let ttfbThreshold = getTtfbThreshold()
export let successRate = new Rate("successful_requests")
export let options = {
  thresholds: {
    "successful_requests": [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
    "http_req_waiting": [`p(90)<${ttfbThreshold}`],
    "http_reqs": [`count>=${rpsThresholds['count']}`]
  }
};

export let projects = getProjects(['name', 'group']);

export function setup() {
  console.log('')
  console.log(`RPS Threshold: ${rpsThresholds['mean']}/s (${rpsThresholds['count']})`)
  console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`)
  console.log(`Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD)*100}%`)
}

export default function() {
  group("API - Project Overview", function() {
    let project = selectProject(projects);

    let params = { headers: { "Accept": "application/json", "PRIVATE-TOKEN": `${__ENV.ACCESS_TOKEN}` } };
    let res = http.get(`${__ENV.ENVIRONMENT_URL}/api/v4/projects/${project['group']}%2F${project['name']}`, params);
    /20(0|1)/.test(res.status) ? successRate.add(true) : (successRate.add(false), logError(res));
  });
}
```

The above script is to test the Projects API, namely to [get the details of a specific project](https://docs.gitlab.com/ee/api/projects.html#get-single-project).

The script does the following:
* Informs `eslint` that global environment variables are to be used.
* Sets some tags describing the test that are used by the tool for filtering tests as well as by GitLab Quality for [reporting](https://gitlab.com/gitlab-org/quality/performance/wikis/current-test-details).
* Imports the various k6 libraries and our own custom modules that we use in the script
* Fails the test if the `ACCESS_TOKEN` environment variable hasn't be set on the machine as this test requires it to authenticate. (more details on the token can be found in the [Running Tests](#running-tests) section)
* Configures the Thresholds that will be used to determine if the test has passed or not (more details on the thresholds can be found in the [Test Results](#test-results) section).
* Loads in the Projects defined in the Environment config file that have the `name` and `group` variables defined. If none found the test is skipped and if multiple is found the test will perform runs against each.
* Logs some useful info in the k6 pre function `setup()` that the tool will use for reporting.
* Finally the main test script itself is last. It selects a Project at random, sets the required headers and then calls the endpoint on the Environment. It then checks if the response was valid, adds the result to the threshold and calls our custom module to report any errors once in the test output.

Note that the above is an example of a typical test. Other [tests](https://gitlab.com/gitlab-org/quality/performance/wikis/current-test-details) may vary in their approach depending on the area being targeted.

#### Test Types

The Tool provides several different types of performance tests that aim to cover the GitLab application. Depending on the type of test you'll see the tool adjust the RPS target accordingly (via adjusting the main RPS target by percentage) as each have a different target throughput based on user patterns.

There are three main types of tests that the Tool provides:
* [`API`](../k6/tests/api) - Tests that target [API](https://docs.gitlab.com/ee/api/) endpoints (RPS target: 100%)
* [`Git`](../k6/tests/git) - Tests that target Git endpoints (RPS target: 10%)
* [`Web`](../k6/tests/web) - Tests that target Web page endpoints (RPS target: 10%)
  * Note that the underlying software this tool is based on, k6, [only tests the response time of the Web page source and doesn't render the page itself](https://docs.k6.io/docs/welcome#section-k6-does-not). This means it won't performance test any HTTP calls found in dynamic scripts, etc... More accurate web page tests could be emulated via [k6's HAR conversion tool](https://docs.k6.io/docs/session-recording-har-support_) but again note this would only measure response time and not rendering time. To measure Rendering time of pages we recommend [sitespeed.io](https://www.sitespeed.io/).

In addition to the above we have experimental [Scenario](../k6/tests/scenarios) tests that aim to represent a complete user scenario (e.g. creating a new issue) and [Quarantined](../k6/tests/quarantined) tests due to some ongoing issue with endpoint or test itself (that don't run unless specifically instructed). More details on the individual tests can be found in the [Test Documentation](test_docs/README.md).

Finally, some specific tests also have individual supporting documentation. These can be found here - [Test Documentation](test_docs/README.md).

#### Unsafe Tests

Some tests provided with the Tool perform [unsafe http requests](https://developer.mozilla.org/en-US/docs/Glossary/safe) (POST, PUT, DELETE or PATCH) and will create data on the environment. As such, these tests are not included by default when running the Tool, with the `--unsafe` flag being provided to include them (refer to the Tools help output above).

These tests are typically fine to run against your environment if you're using the recommended `gitlabhq` project or any other project that can be easily removed after testing has completed. It's unrecommended to run these tests against a permanent project or a live GitLab environment.

## Running the Tests with the Tool

When all of the above configuration is in place the tests can be run with the tool. This can be done in one of two ways - With our [Docker image](https://hub.docker.com/r/gitlab/gitlab-performance-tool) (recommended) or natively on a Linux based system.

### Docker (Recommended)

The recommended way to run the Tool is with our Docker image, [gitlab/gitlab-performance-tool](https://hub.docker.com/r/gitlab/gitlab-performance-tool) (alternative mirror: [registry.gitlab.com/gitlab-org/quality/performance/gitlab-performance-tool](https://gitlab.com/gitlab-org/quality/performance/container_registry)), which will be regularly updated with new features and tests. It can also be used in offline environments.

The image will start running the tests when it's called. The full options for running the tool can be seen by getting the help output by running `docker run -it gitlab/gitlab-performance-tool --help`:

```
GitLab Performance Tool (GPT) v1.2.1 - Performance test runner for GitLab environments based on k6

Documentation: https://gitlab.com/gitlab-org/quality/performance/blob/master/docs/README.md

Usage: run-k6 [options]
Options:
  -h, --help               Show this help message
  -e, --environment=<s>    Name of Environment Config file in environments directory that the test(s) will be run with. Alternative filepath can also be given.
  -o, --options=<s>        Name of Options Config file in options directory that the test(s) will be run with. Alternative filepath can also be given. (Default: 20s_2rps.json)
  -t, --tests=<s+>         Names of Test files or directories to run with. When directory given tests will be recursively added from api, web and git subdirs. (Default: tests)
  -s, --scenarios          Include any tests inside the test directory's scenarios subfolder when true.
  -l, --latency=<i>        Specify a network latency time in MS to increase applicable test thresholds by. Only typically used in slow network environments. (Default: 0)
  -q, --quarantined        Include any tests inside the test directory's quarantined subfolder when true.
  -x, --excludes=<s+>      List of words used to exclude tests by matching against their names. (Default: )
  -u, --unsafe             Include any tests that perform unsafe requests (POST, PUT, DELETE, PATCH)

Environment Variable(s):
  ACCESS_TOKEN             A valid GitLab Personal Access Token for the specified environment that's required by various tests. The token should come from a User that has admin access for
the project(s) to be tested and have API, read_repository, and write_repository permissions. (Default: nil)

Examples:
  Run all Tests with the 60s_200rps Options file against the 10k Environment:
    ./bin/run-k6 --environment 10k.json --options 60s_200rps.json
  Run all API Tests with the 60s_200rps Options file against the 10k Environment:
    ./bin/run-k6 --environment 10k.json --options 60s_200rps.json --tests api
  Run a specific Test with the 60s_200rps Options file against the 10k Environment:
    ./bin/run-k6 --environment 10k.json --options 60s_200rps.json --tests api_v4_groups_projects.js
  -v, --version            Print version and exit
```

The only further setup required to run the tests is to provide any of your custom Environment, Options and Test files to the container via volume mounts as well as provide a mount for results to be saved to on the host. 

Here's an example of how you would run the Docker image with all pieces of config being passed (replacing placeholders as appropriate):

```
docker run -it -e ACCESS_TOKEN=<TOKEN> -v <HOST ENVIRONMENT CONFIG FOLDER>:/environments -v <HOST OPTIONS CONFIG FOLDER>:/options -v <HOST TESTS FOLDER>:/tests -v <HOST RESULTS FOLDER>:/results gitlab/gitlab-performance-tool --environment <ENV FILE NAME>.json --options 60s_500rps.json --tests api_v4_groups_projects.js
```

After running the results will be in the folder you mounted. More details on the results can be found in the [Test Output and Results](#test-output-and-results).

### Linux

You can also run the tool natively on a Linux machine with some caveats:

* The tool has been tested on Debian and Alpine based distros (but it should be able to run on others as well).
* This method will require the machine running the tool to have internet access to install Ruby Gems and k6 (if not already present).

Before running some setup is required for the tool:

1. That [Git LFS](https://git-lfs.github.com/) is installed and any LFS data is confirmed pulled via `git lfs pull`.
1. First, set up [`Ruby`](https://www.ruby-lang.org/en/documentation/installation/) and [`Ruby Bundler`](https://bundler.io) if they aren't already available on the machine.
1. Next, install the required Ruby Gems via Bundler
    * `bundle install`

Next you would need to add any of your custom Environment, Options and Test files to their respective directories. From the tool's root folder this would be `k6/environments`, `k6/options` and `k6/tests` respectively.

Once setup is done you can run the tool with the `bin/run-k6` script. The options for running the tests are the same as when running in Docker but the examples change to the following:

```
Examples:
  Run all Tests with the 60s_200rps Options file against the 10k Environment:
    ./bin/run-k6 --environment 10k.json --options 60s_200rps.json
  Run all API Tests with the 60s_200rps Options file against the 10k Environment:
    ./bin/run-k6 --environment 10k.json --options 60s_200rps.json --tests api
  Run a specific Test with the 60s_200rps Options file against the 10k Environment:
    ./bin/run-k6 --environment 10k.json --options 60s_200rps.json --tests api_v4_groups_projects.js
```

After running the results will be in the tool's `k6/results` folder. More details on the results can be found in the [Test Output and Results](#test-output-and-results).

### Location and Network conditions

Two factors that can significantly affect the Tool's results are the conditions it's run in - Namely Location (physically in relation to the GitLab Environment) and Network.

The GPT is designed to test the GitLab application's server performance directly at max throughputs. As such, it's recommended to be run as close as possible physically to the GitLab environment and in optimum network conditions. Through this, the results given by the Tool can be trusted to be representative of the application's actual performance and not disrupted by location or network issues. If this isn't possible the Tool provides the option to specify a network latency to take into account when it's processing the results, see the `--latency` option description in the `bin/run-k6` script's help output above for more info.

That said, the GPT could also be utilized to test your location and network conditions as a secondary test. For example, you could run the Tool from a desired location (and network conditions) with a representative throughput (i.e. the amount of users that would be typical for that location). In this example this would be run after doing the primary test as described above then compared to validate that your location and network conditions are also performing as expected.

### Test Output and Results

After starting the tool you will see it running each test in order. As an example this, the follow is a capture of the output for the [api_v4_groups_projects](../k6/tests/api/api_v4_groups_projects.js) test:

[![asciicast](https://asciinema.org/a/sxykSpecELOoa2t2DWligcY1s.svg)](https://asciinema.org/a/sxykSpecELOoa2t2DWligcY1s?autoplay=1)

Once all tests have completed you will be presented with a results summary. As an example, here is a test summary for all tests done against the `10k` environment:

```
* Environment:                10k
* Environment Version:        12.7.0-pre `27e32843c50`
* Option:                     60s_200rps
* Date:                       2020-02-06
* Run Time:                   43m 20.28s (Start: 14:16:29 UTC, End: 14:59:49 UTC)
* GPT Version:                v1.1.1

█ Overall Results Score: 97.28%

NAME                                                     | RPS   | RPS RESULT           | TTFB AVG  | TTFB P90             | REQ STATUS     | RESULT
---------------------------------------------------------|-------|----------------------|-----------|----------------------|----------------|-----------------
api_v4_groups_group                                      | 200/s | 194.67/s (>160.00/s) | 142.60ms  | 164.89ms (<500ms)    | 100.00% (>95%) | Passed
api_v4_groups_projects                                   | 200/s | 194.55/s (>160.00/s) | 145.70ms  | 172.15ms (<500ms)    | 100.00% (>95%) | Passed
api_v4_projects_deploy_keys                              | 200/s | 196.1/s (>160.00/s)  | 35.14ms   | 36.32ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_languages                                | 200/s | 195.92/s (>160.00/s) | 32.14ms   | 33.05ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_merge_requests                           | 200/s | 185.58/s (>128.00/s) | 509.51ms  | 1047.50ms (<2000ms)  | 100.00% (>95%) | Passed¹
api_v4_projects_merge_requests_merge_request             | 200/s | 194.75/s (>160.00/s) | 87.78ms   | 94.99ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_merge_requests_merge_request_changes     | 200/s | 195.73/s (>160.00/s) | 65.68ms   | 70.23ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_merge_requests_merge_request_commits     | 200/s | 195.53/s (>160.00/s) | 52.39ms   | 55.33ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_merge_requests_merge_request_discussions | 200/s | 102.5/s (>80.00/s)   | 1758.99ms | 2744.76ms (<4000ms)  | 100.00% (>95%) | Passed¹
api_v4_projects_pagination_keyset                        | 200/s | 193.62/s (>160.00/s) | 164.90ms  | 215.55ms (<500ms)    | 100.00% (>95%) | Passed
api_v4_projects_pagination_offset                        | 200/s | 194.33/s (>160.00/s) | 166.24ms  | 208.63ms (<500ms)    | 100.00% (>95%) | Passed
api_v4_projects_project                                  | 200/s | 195.05/s (>160.00/s) | 87.21ms   | 94.57ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_project_pipelines                        | 200/s | 195.9/s (>160.00/s)  | 48.39ms   | 51.47ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_project_search_blobs                     | 200/s | 193.53/s (>160.00/s) | 129.35ms  | 140.92ms (<500ms)    | 100.00% (>95%) | Passed
api_v4_projects_project_services                         | 200/s | 196.05/s (>160.00/s) | 33.05ms   | 34.29ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_branches                      | 200/s | 41.95/s (>8.00/s)    | 4064.67ms | 7927.96ms (<15000ms) | 100.00% (>95%) | Passed¹
api_v4_projects_repository_branches_branch               | 200/s | 195.92/s (>160.00/s) | 50.28ms   | 53.38ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_commits                       | 200/s | 195.87/s (>160.00/s) | 51.19ms   | 53.47ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_commits_sha                   | 200/s | 195.83/s (>160.00/s) | 50.95ms   | 53.37ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_commits_sha_diff              | 200/s | 195.83/s (>160.00/s) | 48.93ms   | 51.01ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_commits_sha_signature         | 200/s | 195.88/s (>160.00/s) | 43.57ms   | 44.92ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_files_file                    | 200/s | 195.58/s (>160.00/s) | 67.24ms   | 71.19ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_files_file_raw                | 200/s | 195.27/s (>160.00/s) | 71.42ms   | 75.32ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_projects_repository_tree                          | 200/s | 195.8/s (>160.00/s)  | 54.75ms   | 61.87ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_user                                              | 200/s | 196.1/s (>160.00/s)  | 26.74ms   | 27.55ms (<500ms)     | 100.00% (>95%) | Passed
api_v4_users                                             | 200/s | 195.93/s (>160.00/s) | 43.59ms   | 45.49ms (<500ms)     | 100.00% (>95%) | Passed
git_ls_remote                                            | 20/s  | 19.58/s (>16.00/s)   | 43.11ms   | 50.55ms (<500ms)     | 100.00% (>95%) | Passed
git_pull                                                 | 20/s  | 19.63/s (>16.00/s)   | 61.22ms   | 94.48ms (<500ms)     | 100.00% (>95%) | Passed
web_group                                                | 20/s  | 19.17/s (>16.00/s)   | 98.64ms   | 157.34ms (<500ms)    | 100.00% (>95%) | Passed
web_project                                              | 20/s  | 19.12/s (>16.00/s)   | 264.66ms  | 319.93ms (<750ms)    | 100.00% (>95%) | Passed
web_project_blob_file                                    | 20/s  | 19.23/s (>16.00/s)   | 192.85ms  | 265.45ms (<500ms)    | 100.00% (>95%) | Passed
web_project_branches                                     | 20/s  | 18.82/s (>9.60/s)    | 829.48ms  | 983.44ms (<1500ms)   | 100.00% (>95%) | Passed¹
web_project_commits                                      | 20/s  | 19.02/s (>16.00/s)   | 468.46ms  | 565.09ms (<1000ms)   | 100.00% (>95%) | Passed¹
web_project_files                                        | 20/s  | 19.18/s (>16.00/s)   | 246.30ms  | 285.28ms (<500ms)    | 100.00% (>95%) | Passed
web_project_merge_request_changes                        | 20/s  | 4.13/s (>1.60/s)     | 4219.64ms | 9288.47ms (<15000ms) | 100.00% (>95%) | Passed¹
web_project_merge_request_commits                        | 20/s  | 18.75/s (>9.60/s)    | 506.19ms  | 798.30ms (<1500ms)   | 100.00% (>95%) | Passed¹
web_project_merge_request_discussions                    | 20/s  | 18.67/s (>11.20/s)   | 868.33ms  | 2246.80ms (<4000ms)  | 100.00% (>95%) | Passed¹
web_project_merge_requests                               | 20/s  | 19.22/s (>16.00/s)   | 224.07ms  | 299.22ms (<500ms)    | 100.00% (>95%) | Passed
web_project_pipelines                                    | 20/s  | 19.22/s (>16.00/s)   | 237.10ms  | 356.59ms (<1000ms)   | 100.00% (>95%) | Passed
web_user                                                 | 20/s  | 19.3/s (>16.00/s)    | 64.25ms   | 101.68ms (<500ms)    | 100.00% (>95%) | Passed

¹ Result covers endpoint(s) that have known issue(s). Threshold(s) have been adjusted to compensate.
```

Going through the output step by step:
* First there are stats about the environment, tests and the GPT version.
* Next is the Overall Results Score of the environment. This value is calculated from all of the test results and presented as a distilled value to show how well the environment performed overall. Typically a well performing environment should be above 90%.
* After the score is the main results table for each test run. In this table each column shows the following:
  * `NAME` - The name of the test run. Matches the filename of the test as found in the [`tests`](../k6/tests) folder
  * `RPS` - The RPS target used during the test.
  * `RPS RESULT` - The RPS achieved along with it's passing threshold.
  * `TTFB AVG` - The average [Time To First Byte](https://en.wikipedia.org/wiki/Time_to_first_byte) (TTFB) in ms.
  * `TTFB P90` - The 90th [percentile](https://en.wikipedia.org/wiki/Percentile_rank) of TTFB along with it's passing threshold.
  * `REQ STATUS` - The percentage of requests made by the test that returned a successful status (HTTP Code 200 / 201 returned) along with it's passing threshold.
  * `RESULT` - The final result of the test based on it's thresholds.
* Finally the output will end with some optional informational notes for the summary depending on how the results went.

Further information on the results output from `k6` can be found over on it's documentation - [Results output](https://docs.k6.io/docs/results-output).

#### Test Thresholds

As described above there are several thresholds that determine if a test has passed or not:

* RPS achieved by the test is above it's threshold. The threshold is typically the target RPS (with a 20% buffer to account for network or other quirks) by default but tests can have their own different thresholds set depending on the area they are testing. Refer to each test to confirm thresholds.
* [Time To First Byte](https://en.wikipedia.org/wiki/Time_to_first_byte) (TTFB) response time is below it's threshold. Typically this is `500ms` or under for the 90th percentile but like RPS this can differ depending on the test.
* That more than 95% of all requests made were successful (HTTP Code 200 / 201 returned).

#### Evaluating Failures

If any of the tests report RPS, TTFB or Status threshold failures these should be evaluated accordingly in line with the following:

* If any of the tests failed but only by a small amount (e.g. within 10% of the threshold) this is likely due to environmental (such as it still starting up) or network conditions such as latency. If further test runs don't report the same errors then these can typically can be ignored.
* If the failures are substantial (e.g. over 50% of the threshold) this would suggest an environment or product issue and further investigation may be required with even possible escalation through the [appropriate channels](https://about.gitlab.com/support/) (e.g. A support ticket or an issue raised against the main GitLab project).

In some cases you may see tests reporting as failures in the Results Summary but no obvious reason why due to it being a summary. When evaluating failures it's recommended that the full test output is analyzed as well. There you will likely find the specific reason for the failure, such as one specific endpoint failing if there was multiple ones being tested.

#### Comparing Results

We post our own results over on this [project's wiki](https://gitlab.com/gitlab-org/quality/performance/wikis/home) for transparency as well as allowing users to compare. 

Currently, you'll find the following results on our Wiki:
* [Latest Results](https://gitlab.com/gitlab-org/quality/performance/wikis/Benchmarks/Latest) - Our automated CI pipelines run multiple times each week and will post their result summaries to the wiki here each time.
* [GitLab Versions](https://gitlab.com/gitlab-org/quality/performance/wikis/Benchmarks/GitLab-Versions) - A collection of performance test results done against several select release versions of GitLab.

## Troubleshooting

In this section we'll detail any known potential problems when running `k6` and how to manage them.

### `socket: too many open files`

You may see `k6` throw the error `socket: too many open files` many times if you're running a test with particularly high amount of virtual users and throughput, e.g. More than 200 users / RPS.

When this happens it's due to the underlying OS having a limit for how many files can be open at one time, also known as file descriptors. This can be fixed by increasing the number of files that are allowed to be open in the OS. How this is done typically is dependent on the type and version of the OS and you should refer to it's documentation. For example though here are links on how to normally do this on [Linux](https://www.tecmint.com/increase-set-open-file-limits-in-linux/) and [Mac OS](https://medium.com/mindful-technology/too-many-open-files-limit-ulimit-on-mac-os-x-add0f1bfddde) respectively. Another workaround is to run the tests in Docker, which typically have higher limits by default.
